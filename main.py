from sense_emu import SenseHat
import time
from threading import Thread, Lock
from asyncio import Event, create_task, run

sense = SenseHat()

red = (255, 0, 0)
green = (0, 255, 0)
blue = (0, 0, 255)
white = (255, 255, 255)
grey = (127, 127, 127)
black = (0, 0, 0)

continue_loop = True

print_in_loop = True
screen_lock = Lock()


async def main():
    global continue_loop

    sense.show_message("KAIXO", back_colour=black, text_colour=green)

    sense.stick.direction_any = joystick_event

    sensor_loop_task = create_task(sensor_thread_execution())
    await sensor_loop_task

    sense.show_message("AGUR!", back_colour=black, text_colour=red)


async def sensor_thread_execution():
    global continue_loop, print_in_loop, screen_lock
    while continue_loop:
        # Get data
        temp = sense.get_temperature() * 8 / 100
        humidity = sense.get_humidity() * 8 / 100
        press = (sense.get_pressure() - 260) * 8 / 1000

        orientation = sense.get_orientation_degrees()
        yaw = orientation['yaw'] * 8 / 360
        pitch = orientation['pitch'] * 8 / 360
        roll = orientation['roll'] * 8 / 360

        # Draw matrix
        pixels = [black for i in range(64)]
        for i in range(8):
            if i % 8 < temp:
                pixels[i] = red
            if i % 8 < press:
                pixels[8+i] = white
            if i % 8 < humidity:
                pixels[16+i] = blue
            if i % 8 < yaw:
                pixels[40+i] = grey
            if i % 8 < pitch:
                pixels[48+i] = grey
            if i % 8 < roll:
                pixels[56+i] = grey

        screen_lock.acquire()
        sense.set_pixels(pixels)
        screen_lock.release()

        time.sleep(0.5)
    erase_screen(black)


def erase_screen(color=black):
    pixels = [color for i in range(64)]
    sense.set_pixels(pixels)


def end_loop():
    global continue_loop
    continue_loop = False
    erase_screen(black)


def joystick_event(event):
    global continue_loop, print_in_loop, screen_lock
    print(f'{event.action} | {event.direction}')

    if event.action == 'released':
        if event.direction == 'middle':
            end_loop()
        else:
            screen_lock.acquire()
            sense.show_message(event.direction, back_colour=black, text_colour=blue)
            screen_lock.release()


if __name__ == "__main__":
    run(main())
