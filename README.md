# Sense HAT + Python

## Prepare virtual environment

* Create venv and activate it.

```console
python3 -m venv ./venv
source venv/bin/activate
```
* you should see ```(venv)``` a the begginging if everything went OK. 
* Install requirements.
```console
(venv) foo@bar: pip install -r requirements.txt
```


## Deactivate python venv
To deactivate the virtual environment just type:
```console
(venv) foo@bar:~$ deactivate
```